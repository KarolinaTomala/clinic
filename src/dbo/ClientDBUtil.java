package dbo;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Klasa ClientDBUtil odpowiada za laczenie sie z baza danych i uzyskiwanie od niej odpowiedzi na zapytania dla zalogowanego klienta.
 *
 * @author Karolina Tomala
 * @version 1
 */
public class ClientDBUtil extends DBUtil {
    /**
     * Obiekt przechowujacy url sluzacy do polaczenia z baza danych.
     */
    private String URL;
    /**
     * Obiekt przechowujacy nazwe uzytkownika wykorzystywana do logowania do bazy.
     */
    private String name;
    /**
     * Obiekt przechowujacy haslo uzytkownika wykorzystywane do logowania do bazy.
     */
    private String password;

    /**
     * Konstruktor trojparametrowy tworzacy obiekt klasy ClientDBUtil, ktorego wsyztskie pola sa nadane przy tworzeniu obiektu zewnetrznie.
     * @param URL (String) Adres url wykorzystywany do polaczenia z baza.
     * @param name (String) Nazwa uzytkownika, wykorzystywana do logowania do bazy.
     * @param password (int) Haslo uzytkownika, wykorzystywane do logowania do bazy.
     */
    public ClientDBUtil(String URL, String name, String password) {
        this.URL = URL;
        this.name = name;
        this.password = password;
    }

    /**
     * Metoda sluzaca do uzyskania listy wizyt dostepnych dla zadanych parametrow.
     * @param enteredInstitution (String) Zadana nazwa instytucji.
     * @param enteredSpecialization (String) Zadana nazwa specjalizacji.
     * @param enteredDate (String) Zadana data wizyty w formacie YYYY-MM-DD.
     * @return (List<Visit>) Lista wizyt odpowiadająca zadanym parametrom wyszukiwania.
     * @throws SQLException
     */
    public List<Visit> getVisitsWithParameters (String enteredInstitution, String enteredSpecialization, String enteredDate) throws SQLException {

        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        List<Visit> visits = new ArrayList<>();

        try {

            conn = DriverManager.getConnection(URL, name, password);

            String sql = "SELECT * FROM visits_view WHERE user_id IS NULL AND institution = '"+ enteredInstitution +"' AND specialization = '"+ enteredSpecialization + "' AND date = '"+ enteredDate +"'  order by time";
            statement = conn.createStatement();

            resultSet = statement.executeQuery(sql);

            LocalDateTime now = LocalDateTime.now();

            while (resultSet.next()) {

                int id = resultSet.getInt("id");
                String institution = resultSet.getString("institution");
                LocalDate date = resultSet.getDate("date").toLocalDate();
                LocalTime time = resultSet.getTime("time").toLocalTime();
                String specialization = resultSet.getString("specialization");
                String doctorName = resultSet.getString("doctor_name");
                String doctorPhone = resultSet.getString("doctor_phone");
                int userId = resultSet.getInt("user_id");

                LocalDateTime visitDate = LocalDateTime.of(date, time);
                boolean isAfterNow = visitDate.isAfter(now);

                visits.add(new Visit(id, institution, date, time, specialization, doctorName, doctorPhone, userId, isAfterNow));
            }

        } finally {
            close(conn, statement, resultSet);
        }

        return visits;

    }

    /**
     * Metoda sluzaca do uzyskania listy wizyt uzytkownika o zadanym id.
     * @param clientId (int) Id klienta dla ktorego maja zostac wyszukane wizyty.
     * @return (List<Visit>) Lista wizyt przypisanych dla klienta o zadanym jako parametr id.
     * @throws SQLException
     */
    public List<Visit> getClientVisits (int clientId) throws SQLException {
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        List<Visit> visits = new ArrayList<>();

        try {

            conn = DriverManager.getConnection(URL, name, password);

            String sql = "SELECT * FROM visits_view WHERE user_id = " + clientId + " order by date desc";
            statement = conn.createStatement();

            resultSet = statement.executeQuery(sql);

            LocalDateTime now = LocalDateTime.now();

            while (resultSet.next()) {

                int id = resultSet.getInt("id");
                String institution = resultSet.getString("institution");
                LocalDate date = resultSet.getDate("date").toLocalDate();
                LocalTime time = resultSet.getTime("time").toLocalTime();
                String specialization = resultSet.getString("specialization");
                String doctorName = resultSet.getString("doctor_name");
                String doctorPhone = resultSet.getString("doctor_phone");
                int userId = resultSet.getInt("user_id");

                LocalDateTime visitDate = LocalDateTime.of(date, time);
                boolean isAfterNow = visitDate.isAfter(now);

                visits.add(new Visit(id, institution, date, time, specialization, doctorName, doctorPhone, userId, isAfterNow));
            }

        } finally {
            close(conn, statement, resultSet);
        }

        return visits;
    }

    /**
     * Metoda sluzaca do zarezerowania wizyty o zadanym id dla klienta o zadanym id.
     * @param visitId (int) Id wizyty, jaka ma zostac zarezerowana.
     * @param userId (int) Id klienta, dla kotego ma zostac zarezerowana wizyta.
     * @throws SQLException
     */
    public void visitReservation (int visitId, int userId) throws SQLException {
        Connection conn = null;
        PreparedStatement statement = null;

        try {

            conn = DriverManager.getConnection(URL, name, password);

            String sql = "UPDATE visits SET user_id = ? WHERE id = ?";

            statement = conn.prepareStatement(sql);
            statement.setInt(1, userId);
            statement.setInt(2, visitId);

            statement.execute();

        } finally {
            close(conn, statement, null);
        }
    }

    /**
     * Metoda sluzaca do anulowania rezerwacji wizyty o zadanym id.
     * @param visitId (int) Id wizyty, ktorej rezewacja ma zostac anulowana.
     * @throws SQLException
     */
    public void cancelVisit (int visitId) throws SQLException {
        Connection conn = null;
        PreparedStatement statement = null;

        try {

            conn = DriverManager.getConnection(URL, name, password);

            String sql = "UPDATE visits SET user_id = NULL WHERE id = ?";

            statement = conn.prepareStatement(sql);
            statement.setInt(1, visitId);

            statement.execute();

        } finally {
            close(conn, statement, null);
        }
    }

    /**
     * Metoda sluzaca do pobrania z bazy nazwy placowki o zadanym id.
     * @param id (int) Id placowki.
     * @return (String) Nazwa placowki o zadanym jako parametr id.
     * @throws SQLException
     */
    public String getInstitutionNameById (int id) throws SQLException {

        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        String InstName = null;

        try {

            conn = DriverManager.getConnection(URL, name, password);

            String sql = "SELECT name FROM institutions WHERE id = " + id;
            statement = conn.createStatement();

            resultSet = statement.executeQuery(sql);

            resultSet.next();
            InstName = resultSet.getString("name");

        } finally {
            close(conn, statement, resultSet);
        }

        return InstName;

    }

    /**
     * Metoda sluzaca do pobrania danych na temat wszystkich dostepnych placowek.
     * @return (List<Institution>) Lista danych na temat placowek.
     * @throws SQLException
     */
    public List<Institution> getInstitutions () throws SQLException {
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        List<Institution> list = new ArrayList<>();

        try {

            conn = DriverManager.getConnection(URL, name, password);

            String sql = "SELECT id, name FROM institutions";
            statement = conn.createStatement();

            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {

                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");

                list.add(new Institution(id, name));
            }

        } finally {
            close(conn, statement, resultSet);
        }

        return list;
    }

    /**
     * Metoda sluzaca do pobrania danych na temat wszystkich dostepnych specjalizacji.
     * @return (List<String>) Lista reprezentujaca nazwy wszystkich dostepnych specjalizacji.
     * @throws SQLException
     */
    public List<String> getSpecializations () throws SQLException {
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        List<String> list = new ArrayList<>();

        try {

            conn = DriverManager.getConnection(URL, name, password);

            String sql = "SELECT name FROM specializations";
            statement = conn.createStatement();

            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {

                String name = resultSet.getString("name");

                list.add(name);
            }

        } finally {
            close(conn, statement, resultSet);
        }

        return list;
    }
}

package dbo;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Klasa reprezentujaca dane na temat wizyty.
 *
 * @author Karolina Tomala
 * @version 1
 */
public class Visit {
    /**
     * Id wizyty.
     */
    private int id;
    /**
     * Nazwa instytucji w jakiej ma miejsce wizyta.
     */
    private String institution;
    /**
     * Data wizyty
     */
    private LocalDate date;
    /**
     * Godzina wizyty.
     */
    private LocalTime time;
    /**
     * Specjalizacja lekarza udzielajacego wizyty.
     */
    private String specialization;
    /**
     * Imie i nazwisko lekarza udzielajacego wizyty.
     */
    private String doctorName;
    /**
     * Numer telefonu lekarza udzielajacego wizyty.
     */
    private String doctorPhone;
    /**
     * Id klienta, ktory zarezerwowal wizyte (null/0, jesli wizyta nie jest zarezerowana).
     */
    private int userId;
    /**
     * Parametr informujacy, czy wizyta miala juz miejsce.
     */
    private boolean isAfterNow;

    /**
     * Konstruktor bezparametrowy tworzacy nowy obiekt klasy Visit, wartosci pol obiektu sa puste (null, 0).
     */
    public Visit() {
    }

    /**
     * Konstruktor 9-parametrowy tworzacy nowy obiekt klasy Visit o zadanych wartosciach pol.
     * @param id (int) Id wizyty.
     * @param institution (String) Nazwa instytucji.
     * @param date (LocalDate) Data wizyty.
     * @param time (LocalTime) Godzina wizyty.
     * @param specialization (String) Nazwa specjalizacji.
     * @param doctorName (String) Imię i nazwisko lekarza.
     * @param doctorPhone (String) Numer telefonu lekarza.
     * @param userId (int) Id klienta, ktory zarezerowala wizyte.
     * @param isAfterNow (boolean) Parametr informujacy, czy wizyta miala juz miejsce.
     */
    public Visit(int id, String institution, LocalDate date, LocalTime time, String specialization, String doctorName, String doctorPhone, int userId, boolean isAfterNow) {
        this.id = id;
        this.institution = institution;
        this.date = date;
        this.time = time;
        this.specialization = specialization;
        this.doctorName = doctorName;
        this.doctorPhone = doctorPhone;
        this.userId = userId;
        this.isAfterNow = isAfterNow;
    }

    /**
     * Metoda sluzaca do uzyskania id wizyty
     * @return (int) Id wizyty.
     */
    public int getId() {
        return id;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola id.
     * @param id (String) Nowa wartosc pola id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metoda sluzaca do uzyskania nazwy instytucji.
     * @return (String) Nazwa instytucji.
     */
    public String getInstitution() {
        return institution;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola institution.
     * @param institution (String) Nowa wartosc pola institution.
     */
    public void setInstitution(String institution) {
        this.institution = institution;
    }

    /**
     * Metoda sluzaca do uzyskania daty wizyty.
     * @return (LocalDate) Data wizyty.
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola date.
     * @param date (String) Nowa wartosc pola date.
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * Metoda sluzaca do uzyskania godziny wizyty.
     * @return (LocalTime) Godzina wizyty.
     */
    public LocalTime getTime() {
        return time;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola time.
     * @param time (String) Nowa wartosc pola time.
     */
    public void setTime(LocalTime time) {
        this.time = time;
    }

    /**
     * Metoda sluzaca do uzyskania nazwy specjalizacji.
     * @return (String) Nazwa specjalizacji.
     */
    public String getSpecialization() {
        return specialization;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola specialization.
     * @param specialization (String) Nowa wartosc pola specialization.
     */
    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    /**
     * Metoda sluzaca do uzyskania imienia i nazwiska lekarza.
     * @return (Sting) Imie i nazwisko lekarza.
     */
    public String getDoctorName() {
        return doctorName;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola doctorName.
     * @param doctorName (String) Nowa wartosc pola doctorName.
     */
    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    /**
     * Metoda sluzaca do uzyskania numeru telefonu lekarza.
     * @return (String) Numer telefonu lekarza.
     */
    public String getDoctorPhone() {
        return doctorPhone;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola doctorPhone.
     * @param doctorPhone (String) Nowa wartosc pola doctorPhone.
     */
    public void setDoctorPhone(String doctorPhone) {
        this.doctorPhone = doctorPhone;
    }

    /**
     * Metoda sluzaca do uzyskania id klienta, ktory zarezerwowal wizyte.
     * @return (int) Id klienta, ktory zarezerwowal wizyte.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola userId.
     * @param userId (String) Nowa wartosc pola userId.
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Metoda sluzaca do uzyskania wartosci parametru informujacego, czy wizyta miala juz miejsce.
     * @return (boolean) Parametr informujacy, czy wizyta miala juz miejsce. Przyjmuje wartosc true jesli wizyta nie miala jeszcze miejsca.
     */
    public boolean getIsAfterNow() {
        return isAfterNow;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola afterNow.
     * @param afterNow (String) Nowa wartosc pola afterNow.
     */
    public void setIsAfterNow(boolean afterNow) {
        isAfterNow = afterNow;
    }
}

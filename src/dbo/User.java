package dbo;

import java.time.LocalDate;

/**
 * Klasa reprezentujaca dane na temat klienta.
 *
 * @author Karolina Tomala
 * @version 1
 */
public class User {
    /**
     * Imię klienta.
     */
    private String name;
    /**
     * Nazwisko klienta.
     */
    private String surname;
    /**
     * Data urodzenia klienta.
     */
    private LocalDate birthDay;
    /**
     * Numer telefonu klienta.
     */
    private String phone;
    /**
     * Adres email klienta.
     */
    private String email;
    /**
     * Hasło klienta.
     */
    private String password;

    /**
     * Konstruktor bezparametrowy tworzacy nowy obiekt klasy User, wartosci pol obiektu sa puste (null).
     */
    public User() {
    }

    /**
     * Konstruktor 6-parametrowy tworzacy nowy obiekt klasy User o zadanych wartosciach pol.
     * @param name (String) Imie klienta.
     * @param surname (String) Nazwisko klienta.
     * @param birthDay (LocalDate) Data urodzenia klienta.
     * @param phone (String) Numer telefonu klienta.
     * @param email (String) Adres email klienta.
     * @param password (String) Haslo klienta.
     */
    public User(String name, String surname, LocalDate birthDay, String phone, String email, String password) {
        this.name = name;
        this.surname = surname;
        this.birthDay = birthDay;
        this.phone = phone;
        this.email = email;
        this.password = password;
    }

    /**
     * Metoda sluzaca do uzyskania imienia klienta.
     * @return (String) Imie klienta.
     */
    public String getName() {
        return name;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola name.
     * @param name (String) Nowa wartosc pola name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Metoda sluzaca do uzyskania nazwiska klienta.
     * @return (String) Nazwisko klienta.
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola surname.
     * @param surname (String) Nowa wartosc pola surname.
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Metoda sluzaca do uzyskania daty urodzenia klienta.
     * @return (LocalDate) Data urodzenia klienta.
     */
    public LocalDate getBirthDay() {
        return birthDay;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola birthDay.
     * @param birthDay (LocalDate) Nowa wartosc pola birthDay.
     */
    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    /**
     * Metoda sluzaca do uzyskania numeru telefonu klienta.
     * @return (String) Numer telefonu klienta.
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola phone.
     * @param phone (String) Nowa wartosc pola phone.
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Metoda sluzaca do uzyskania adresu email klienta.
     * @return (String) Adres email klienta.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola email.
     * @param email (String) Nowa wartosc pola email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Metoda sluzaca do uzyskania hasla klienta.
     * @return (String) Haslo klienta.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola password.
     * @param password (String) Nowa wartosc pola password.
     */
    public void setPassword(String password) {
        this.password = password;
    }
}

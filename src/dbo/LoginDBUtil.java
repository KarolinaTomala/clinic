package dbo;

import javax.sql.DataSource;
import java.sql.*;

/**
 * Klasa LoginDBUtil odpowiada za laczenie sie z baza danych i uzyskiwanie od niej odpowiedzi na zapytania potrzebne do zalogowania lub utworzenia nowego klienta.
 *
 * @author Karolina Tomala
 * @version 1
 */
public class LoginDBUtil extends DBUtil {

    /**
     * Obiekt pozwalajcy uzyskac dane na temat parametrow niezbednych do polaczenia z baza.
     */
    private DataSource dataSource;

    /**
     * Konstruktor jednoparametrowy tworzacy nowy obiekt klasy LoginDBUtil.
     * @param dataSource (DataSource)
     */
    public LoginDBUtil(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Metoda pozwalajaca na pobranie z bazy hasla klienta o zadanym adresie email.
     * @param email (String) Adres email, dla ktorego ma zostac uzyskane haslo.
     * @return (String) Zakodowane haslo dla zadanego adresu email; null jesli zadanego adresu email nie ma w bazie.
     */
    public String getPassword(String email) {
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String password;

        try {

            conn = dataSource.getConnection();

            String sql = "SELECT get_password(\'" + email + "\') AS password";
            statement = conn.createStatement();

            resultSet = statement.executeQuery(sql);

            resultSet.next();
            password = resultSet.getString("password");

        }
        catch (Exception e) {
            e.printStackTrace();
            password = null;
        }
        finally {
            close(conn, statement, resultSet);
        }

        return password;
    }

    /**
     * Metoda pozwalajaca na pobranie z bazy id klienta dla zadanego adresu email.
     * @param email (String) Adres email, dla ktorego ma zostac uzyskane id.
     * @return (int) Numer id klienta, do ktorego przypisany jest zadany adres email.
     * @throws SQLException
     */
    public int getId(String email) throws SQLException {
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;
        int id;

        try {

            conn = dataSource.getConnection();

            String sql = "SELECT get_id(\'" + email + "\') AS id";
            statement = conn.createStatement();

            resultSet = statement.executeQuery(sql);

            resultSet.next();
            id = resultSet.getInt("id");

        } finally {
            close(conn, statement, resultSet);
        }

        return id;
    }

    /**
     * Metoa sprawdzajaca, czy zadany adres email istnieje w bazie.
     * @param email (String) Adres email.
     * @return (boolean) True jesli zadany email jest w bazie; false jesli zadanego adresu nie ma w bazie.
     * @throws SQLException
     */
    public boolean isEmailInDatabase (String email) throws SQLException {
        boolean check = true;

        int id = getId(email);

        if (id == 0) {
            check = false;
        }

        return check;
    }

    /**
     * Metoda pozwalajaca na dodanie nowego klienta do bazy danych.
     * @param user (User) Obiekt reprezentujacy atrybuty nowego klienta.
     * @throws SQLException
     */
    public void addUser (User user) throws SQLException {
        Connection conn = null;
        PreparedStatement statement = null;

        try {
            conn = dataSource.getConnection();
            String sql = "INSERT INTO users(name, surname, birth_day, phone, email, password) " +
                    "VALUES(?,?,?,?,?,?)";

            statement = conn.prepareStatement(sql);
            statement.setString(1, user.getName());
            statement.setString(2, user.getSurname());
            statement.setDate(3,  Date.valueOf(user.getBirthDay().toString()));
            statement.setString(4, user.getPhone());
            statement.setString(5, user.getEmail());
            statement.setString(6, user.getPassword());

            statement.execute();

        } finally {
            close(conn, statement, null);
        }

    }
}

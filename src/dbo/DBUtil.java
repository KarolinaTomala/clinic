package dbo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
/**
 * Klasa DBUtil odpowiada za laczenie sie z baza danych i uzyskiwanie od niej odpowiedzi na zapytania.
 *
 * @author Karolina Tomala
 * @version 1
 */
public class DBUtil {

    /**
     * Metoda sluzaca do zamkniecia połaczenia z baza danych.
     * @param conn (Connection)
     * @param statement (Statement)
     * @param resultSet (ResultSet)
     */
    protected static void close(Connection conn, Statement statement, ResultSet resultSet) {

        try {

            if (resultSet != null)
                resultSet.close();

            if (statement != null)
                statement.close();

            if (conn != null)
                conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

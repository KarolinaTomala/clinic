package dbo;

/**
 * Klasa reprezentujaca dane na temat placowek.
 *
 * @author Karolina Tomala
 * @version 1
 */
public class Institution {
    /**
     * Obiekt przechowujace id placowki.
     */
    private int id;
    /**
     * Obiekt przechowujacy nazwe placowki.
     */
    private String name;

    /**
     * Konstruktor bezparametrowy tworzacy nowy obiekt klasy Institution z domyslnymi wartosciami pol (id rowne zero, nazwa bedace nullem).
     */
    public Institution() {
    }

    /**
     * Konstruktor dwuparametrowy tworzacy nowy obiekt klasy Institution z zadanymi wartosciami pol.
     * @param id (int) Id placowki.
     * @param name (String) Nazwa placowki.
     */
    public Institution(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Metoda sluzaca do pobrania id placowki.
     * @return (int) Id placowki.
     */
    public int getId() {
        return id;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola id.
     * @param id (int) Nowe wartosc pola id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metoda sluzaca do pobrania nazwy placowki.
     * @return (String) Nazwa placowki.
     */
    public String getName() {
        return name;
    }

    /**
     * Metoda sluzaca do zmiany wartosci pola name.
     * @param name (String) Nowa wartosc pola name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Metoda sluzaca do wyswietlenia danych na temat placowki jako String, zwracajaca nazwe placowki.
     * @return (String) Nazwa placowki.
     */
    @Override
    public String toString() {
        return name;
    }
}

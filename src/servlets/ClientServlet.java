package servlets;

import dbo.ClientDBUtil;
import dbo.Institution;
import dbo.Visit;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Klasa dziedziczaca po klasie HttpServlet, odpowiedzialna za obsluge zapytan GET oraz POST protokolu HTTP wywolywanych przez klienta.
 *
 * @author Karolina Tomala
 * @version 1
 */
@WebServlet("/ClientServlet")
public class ClientServlet extends HttpServlet {

    /**
     * Obiekt odpowidzialny za nawiazywanie polaczen z baza i uzyskiwanie z niej informacji.
     */
    private ClientDBUtil dbUtil;
    /**
     * Obiekt przechowujacy nazwe uzytkownika wykorzystywana do logowania do bazy.
     */
    private String name = "clientClinic";
    /**
     * Obiekt przechowujacy haslo uzytkownika wykorzystywane do logowania do bazy.
     */
    private String pass = "client";
    /**
     * Obiekt przechowujacy url sluzacy do polaczenia z baza danych.
     */
    private String db_url = "jdbc:mysql://localhost:3306/clinic?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET";

    /**
     * Metoda inicjujaca servlet oraz tworzaca nowy obiekt pozwalajacy na uzyskanie polaczenia z baza danych.
     * @param config (ServletConfig)
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        try {
            dbUtil = new ClientDBUtil(db_url, name, pass);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    /**
     * Metoda sluzaca do obslugi zapytania POST protokolu HTTP, wywolujaca wyswietlanie danych dla zalagowanego uzytkownika w panelu klienta.
     * @param request (HttpServletRequest)
     * @param response (HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html");

        try {
            listData(request, response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda sluzaca do obslugi zapytania GET protokolu HTTP, pozwalajaca na rozna reakcje Servletu w zaleznosci od wartosci parametru zapytania o nazwie command.
     * @param request (HttpServletRequest)
     * @param response (HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

        try {

            String command = request.getParameter("command");

            if (command == null)
                command = "LIST";

            switch (command) {
                case "LIST":
                    listData(request, response);
                    break;

                case "CANCEL":
                    cancelVisit(request, response);
                    break;

                case "RESERVATION":
                    listData(request, response);
                    break;

                case "RETURN":
                    listData(request, response);
                    break;

                case "LOGOUT":
                    logout(request, response);
                    break;

                default:
                    listData(request, response);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda sluzaca do realizacji operacji anulowania wizyty przez zalagowanego uzytkownika.
     * @param request (HttpServletRequest)
     * @param response (HttpServletResponse)
     */
    private void cancelVisit (HttpServletRequest request, HttpServletResponse response) {
        int visitId = Integer.valueOf(request.getParameter("visitId"));

        try {
            dbUtil.cancelVisit(visitId);
            listData(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda sluzaca do wyswietlenia danych w panelu klienta dla zalagowanego uzytkownika.
     * @param request (HttpServletRequest)
     * @param response (HttpServletResponse)
     */
    private void listData(HttpServletRequest request, HttpServletResponse response) throws Exception {

         int userId;

         try {
             userId = Integer.valueOf(request.getSession().getAttribute("userId").toString());
         }
         catch (NullPointerException e) {
             userId = 0;
         }


        if (userId > 0) {
            List<Visit> visits = dbUtil.getClientVisits(userId);
            request.setAttribute("CLIENT_VISITS_LIST", visits);

            if (visits.isEmpty()) {
                request.setAttribute("message", "Brak wizyt");
            }
            else {
                request.setAttribute("message", "");
            }

            List<Institution> institutions = dbUtil.getInstitutions();
            request.setAttribute("INSTITUTIONS_LIST", institutions);

            List<String> specializations = dbUtil.getSpecializations();
            request.setAttribute("SPECIALIZATIONS_LIST", specializations);

            RequestDispatcher dispatcher = request.getRequestDispatcher("/client_panel.jsp");
            dispatcher.forward(request, response);
        }
        else {
            redirectToLoginForm(request, response);
        }

    }

    /**
     * Metoda sluzaca do wylogowania zalagowanego uzytkownika.
     * @param request (HttpServletRequest)
     * @param response (HttpServletResponse)
     */
    private void logout (HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getSession().setAttribute("userId", 0);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/login_form.jsp");
            dispatcher.forward(request, response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda sluzaca do przekierowania niezalogowanego uzytkownika do formularza logowania.
     * @param request (HttpServletRequest)
     * @param response (HttpServletResponse)
     */
    private void redirectToLoginForm (HttpServletRequest request, HttpServletResponse response) {
        try {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/login_form.jsp");
            dispatcher.forward(request, response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


}

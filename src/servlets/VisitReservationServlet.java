package servlets;

import dbo.ClientDBUtil;
import dbo.Visit;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;

/**
 * Klasa dziedziczaca po klasie HttpServlet, odpowiedzialna za obsluge zapytan GET oraz POST protokolu HTTP wywolywanych przez zalogowanego klienta w celu rezerwacji nowej wizyty.
 *
 * @author Karolina Tomala
 * @version 1
 */
@WebServlet("/VisitReservationServlet")
public class VisitReservationServlet extends HttpServlet {

    /**
     * Obiekt odpowidzialny za nawiazywanie polaczen z baza i uzyskiwanie z niej informacji.
     */
    private ClientDBUtil dbUtil;
    /**
     * Obiekt przechowujacy nazwe uzytkownika wykorzystywana do logowania do bazy.
     */
    private String name = "clientClinic";
    /**
     * Obiekt przechowujacy haslo uzytkownika wykorzystywane do logowania do bazy.
     */
    private String pass = "client";
    /**
     * Obiekt przechowujacy url sluzacy do polaczenia z baza danych.
     */
    private String db_url = "jdbc:mysql://localhost:3306/clinic?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET";

    /**
     * Metoda inicjujaca servlet oraz tworzaca nowy obiekt pozwalajacy na uzyskanie polaczenia z baza danych.
     * @param config (ServletConfig)
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        try {
            dbUtil = new ClientDBUtil(db_url, name, pass);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    /**
     * Metoda sluzaca do obslugi zapytania POST protokolu HTTP, wywolujaca wyswietlanie danych na temat dostepnych wizyt o zadanych parametrach dla zalagowanego uzytkownika.
     * @param request (HttpServletRequest)
     * @param response (HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

        response.setContentType("text/html");

        try {

            int enteredInstitutionId = Integer.valueOf(request.getParameter("institution"));
            String enteredInstitution = dbUtil.getInstitutionNameById (enteredInstitutionId);
            String enteredSpecialization = request.getParameter("specialization");
            String eneteredDate = request.getParameter("visitDate");

            List<Visit> visits = dbUtil.getVisitsWithParameters(enteredInstitution, enteredSpecialization, eneteredDate);
            request.setAttribute("VISITS_LIST", visits);

            if (visits.isEmpty()) {
                request.setAttribute("message", "Brak wizyt o podanych parametrach.");
            }
            else {
                request.setAttribute("message", "");
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("/visit_panel.jsp");
            dispatcher.forward(request, response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda sluzaca do obslugi zapytania GET protokolu HTTP, pozwalajaca na rozna reakcje Servletu w zaleznosci od wartosci parametru zapytania o nazwie command.
     * @param request (HttpServletRequest)
     * @param response (HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {

        try {

            String command = request.getParameter("command");

            switch (command) {

                case "RESERVATION":
                    makeReservation(request, response);
                    break;

                case "RETURN":
                    returnToClientPanel(request, response);
                    break;

                default:
                    returnToClientPanel(request, response);
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    /**
     * Metoda sluzaca do zarezerwowania wybranej wizyty przez zalogowanego uzytkownika.
     * @param request (HttpServletRequest)
     * @param response (HttpServletResponse)
     */
    private void makeReservation (HttpServletRequest request, HttpServletResponse response) {

        int userId = Integer.valueOf(request.getSession().getAttribute("userId").toString());

        if (userId > 0) {
            try {

                int visitId = Integer.valueOf(request.getParameter("visitId"));

                dbUtil.visitReservation(visitId, userId);

                returnToClientPanel(request, response);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            redirectToLoginForm(request, response);
        }
    }

    /**
     * Metoda sluzaca do przekierowania zalogowanego uzytkownika do panelu klienta.
     * @param request (HttpServletRequest)
     * @param response (HttpServletResponse)
     */
    private void returnToClientPanel (HttpServletRequest request, HttpServletResponse response) {
        try {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientServlet");
            dispatcher.forward(request, response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda sluzaca do przekierowania niezalogowanego uzytkownika do formularza logowania.
     * @param request (HttpServletRequest)
     * @param response (HttpServletResponse)
     */
    private void redirectToLoginForm (HttpServletRequest request, HttpServletResponse response) {
        try {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/login_form.jsp");
            dispatcher.forward(request, response);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}

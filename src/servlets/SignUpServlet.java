package servlets;

import dbo.LoginDBUtil;
import dbo.User;
import org.mindrot.jbcrypt.BCrypt;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;
import java.time.LocalDate;

/**
 * Klasa dziedziczaca po klasie HttpServlet, odpowiedzialna za obsluge zapytań POST protokołu HTTP wywolywanych w celu rejestracji nowego uzytkownika.
 *
 * @author Karolina Tomala
 * @version 1
 */
@WebServlet("/SignUpServlet")
public class SignUpServlet extends HttpServlet {

    /**
     * Obiekt pozwalajcy uzyskac dane na temat parametrow niezbednych do polaczenia z baza.
     */
    private DataSource dataSource;
    /**
     * Obiekt odpowidzialny za nawiazywanie polaczen z baza i uzyskiwanie z niej informacji.
     */
    private LoginDBUtil dbUtil;

    /**
     * Konstruktor bezparametrowy tworzacy nowy obiekt klasy SignUpServlet.
     */
    public SignUpServlet() {

        Context initCtx = null;
        try {
            initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:comp/env");

            dataSource = (DataSource)
                    envCtx.lookup("jdbc/clinic_web_app");

        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda inicjujaca servlet oraz tworzaca nowy obiekt pozwalajacy na uzyskanie polaczenia z baza danych.
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        super.init();

        try {

            dbUtil = new LoginDBUtil(dataSource);

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    /**
     * Metoda sluzaca do obslugi zapytania POST protokolu HTTP, pozwalajaca na zarejestrowanie nowego uzytkownika na podstawie parametrow podanych w formularzu, sprawdzajaca, czy podane parametry sa poprawne i kierujaca uzytkownika do odpowiedniego okna komunikatu powodzenia lub niepowodzenia rejestracji.
     * @param request (HttpServletRequest)
     * @param response (HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        LocalDate birthDay = LocalDate.parse(request.getParameter("birthDay"));
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        System.out.println(email);
        String password = request.getParameter("password");
        System.out.println(password);

        String passwordHashed = BCrypt.hashpw(password, BCrypt.gensalt(10));

        User newUser = new User(name, surname, birthDay, phone, email, passwordHashed);

        int age = LocalDate.now().getYear() -  birthDay.getYear();

        try {
            if ((!dbUtil.isEmailInDatabase(email)) && (age>=18)) {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/sign_up_successful.jsp");
                dbUtil.addUser(newUser);
                dispatcher.forward(request, response);

            } else {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/sign_up_fail.jsp");
                dispatcher.include(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

package servlets;

import dbo.LoginDBUtil;
import org.mindrot.jbcrypt.BCrypt;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.sql.DataSource;
import java.io.IOException;

/**
 * Klasa dziedziczaca po klasie HttpServlet, odpowiedzialna za obsluge zapytan POST protokolu HTTP wywolywanych w celu zalogowania uzytkownika.
 *
 * @author Karolina Tomala
 * @version 1
 */
@WebServlet("/LoginServlet")
public class LoginServlet  extends HttpServlet {

    /**
     * Obiekt pozwalajcy uzyskac dane na temat parametrow niezbednych do polaczenia z baza.
     */
    private DataSource dataSource;
    /**
     * Obiekt odpowidzialny za nawiazywanie polaczen z baza i uzyskiwanie z niej informacji.
     */
    private LoginDBUtil dbUtil;

    /**
     * Konstruktor bezparametrowy tworzacy nowy obiekt klasy LoginServlet.
     */
    public LoginServlet() {

        Context initCtx = null;
        try {
            initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:comp/env");

            dataSource = (DataSource)
                    envCtx.lookup("jdbc/clinic_web_app");

        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda inicjujaca servlet oraz tworzaca nowy obiekt pozwalajacy na uzyskanie polaczenia z baza danych.
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        super.init();

        try {

            dbUtil = new LoginDBUtil(dataSource);

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    /**
     * Metoda sluzaca do obslugi zapytania POST protokolu HTTP, pozwalajaca na walidacje podanego w formularzu logowania adresu email i hasla i kierujaca uzytkownika do panelu klienta lub okna infromujacego o bledzie logowania.
     * @param request (HttpServletRequest)
     * @param response (HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String email = request.getParameter("emailInput");
        String password = request.getParameter("passwordInput");

        if (validate(email, password)) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientServlet");

            try {
                int clientId = dbUtil.getId(email);
                HttpSession session = request.getSession();
                session.setAttribute("userId", clientId);
                dispatcher.forward(request, response);
            }
            catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/login_fail.jsp");
            dispatcher.include(request, response);
        }
    }

    /**
     * Metoda sluzaca do sprawdzenia poprawnosci hasla dla zadanego przez klienta adresu email.
     * @param email (String) Wprowadzony w formularzu adres email.
     * @param password (String) Wprowadzone w formularzu hasło.
     * @return (boolean) True jesli walidacja powiodla sie, w przecinym wypadku false.
     */
    private boolean validate(String email, String password) {
        boolean isCorrect = false;

        String dbPassword = dbUtil.getPassword(email);

        if (dbPassword != null) {
            if (BCrypt.checkpw(password, dbPassword))
                isCorrect = true;
        }

        return isCorrect;
    }
}

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- link rel="stylesheet" href="css/bootstrap.min.css" -->
    <link rel="stylesheet" href="css/bootstrap-cosmo.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Przychodnia Zdrówko</a>

    <div id="navbarColor02">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.jsp">Strona główna</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="institutions.jsp">Nasze przychodnie</a>
            </li>
            <li class="nav-item">
                <c:url var="listLink" value="ClientServlet">
                    <c:param name="command" value="LIST"></c:param>
                </c:url>
                <a class="nav-link" href="${listLink}">Panel klienta</a>
            </li>
        </ul>
    </div>
</nav>

<div class="jumbotron">
    <div class="container">
        <h1>Zdrowie szyte na miarę</h1>
        <p>Zdrówko to sieć przychodni, w której zadbamy o Ciebie i Twoich bliskich. </br> Bez kolejek, dostarczamy usługi na najwyższym poziomie w trosce o Twoje zdrowie!</p>
        <p>Sprawdź gdzie możesz znaleźć nasze placówki:</p>
        <p><a class="btn btn-primary btn-lg" href="institutions.jsp" role="button">Nasze placówki &raquo;</a></p>
    </div>
</div>

<div class="container margin_bottom_30">
    <div class="row">
        <div class="col-md-4 text-center">
            <h2>Najwyższa jakość</h2>
            <p>Nasza sieć przychodni stawia na najwyższą jakość oferowanych usług. Naszym priorytetem jest zapewnienie klientom jak dostępu najlepszych specjalistów w najlepszej cenie.</p>
            <p><img class="icons" src="img/quality.png"></p>
        </div>
        <div class="col-md-4 text-center">
            <h2>Wszechstronność</h2>
            <p>W sieci naszych przychodni pracują lekarze z różnych dziedzin, dokładając starań, by zapewnić Ci najlepszą obłsugę niezależnie od dolegliwości.</p>
            <p><img class="icons" src="img/laptop.png"></p>
        </div>
        <div class="col-md-4 text-center">
            <h2>Nowoczesność</h2>
            <p>Nasze przychodnie oferują dostęp do najlepszeog sprzętu medycznego a także łatwą rejestrację bez kolejek i długiego czekania na połączenie telefoniczne!</p>
            <p><img class="icons" src="img/atom.png"></p>
        </div>
    </div>

</div>

<footer>
    By Karolina Tomala, 2020
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/main.js"></script>
</body>
</html>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- link rel="stylesheet" href="css/bootstrap.min.css" -->
    <link rel="stylesheet" href="css/bootstrap-cosmo.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Przychodnia Zdrówko</a>

    <div id="navbarColor02">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.jsp">Strona główna</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="institutions.jsp">Nasze przychodnie</a>
            </li>
            <li class="nav-item">
                <c:url var="listLink" value="ClientServlet">
                    <c:param name="command" value="LIST"></c:param>
                </c:url>
                <a class="nav-link" href="${listLink}">Panel klienta</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container margin_bottom_30">
    <div class="row clinics_rows">
        <div class="col-md-6">
            <h2>Przychodnia Zdrówko (Wrocław, Śliczna)</h2>
            <p></p>
            <h4>Dostępni specjaliści:</h4>
            <ul>
                <li>Lekarz rodzinny</li>
                <li>Dermatolog</li>
                <li>Diabetolog</li>
                <li>Endokrynolog</li>
                <li>Okulista</li>
            </ul>
            <h4>Dane kontaktowe:</h4>
            <p><span class="glyphicon glyphicon-globe" aria-hidden="true"></span>   ul. Śliczna 22, Wrocław</p>
            <p><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>   123456789</p>
        </div>
        <div class="col-md-6">
            <img class="respons_img" src="img/clinic1.jpg">
        </div>
    </div>

    <div class="row clinics_rows">
        <div class="col-md-6">
            <img class="respons_img" src="img/clinic2.jpg">
        </div>
        <div class="col-md-6">
            <h2>Przychodnia Zdrówko (Wrocław, Różana)</h2>
            <p></p>
            <h4>Dostępni specjaliści:</h4>
            <ul>
                <li>Lekarz rodzinny</li>
                <li>Endokrynolog</li>
            </ul>
            <h4>Dane kontaktowe:</h4>
            <p><span class="glyphicon glyphicon-globe" aria-hidden="true"></span>   ul. Różana 32, Wrocław</p>
            <p><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>   123456788</p>
        </div>
    </div>

    <div class="row clinics_rows">
        <div class="col-md-6">
            <h2>Przychodnia Zdrówko (Warszawa, Topolowa)</h2>
            <p></p>
            <h4>Dostępni specjaliści:</h4>
            <ul>
                <li>Lekarz rodzinny</li>
                <li>Dermatolog</li>
                <li>Diabetolog</li>
                <li>Okulista</li>
            </ul>
            <h4>Dane kontaktowe:</h4>
            <p><span class="glyphicon glyphicon-globe" aria-hidden="true"></span>   ul. Topolowa 17B, Warszawa</p>
            <p><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>   123456787</p>
        </div>
        <div class="col-md-6">
            <img class="respons_img" src="img/clinic3.jpg">
        </div>
    </div>

    <div class="row clinics_rows">
        <div class="col-md-6">
            <img class="respons_img" src="img/clinic4.jpg">
        </div>
        <div class="col-md-6">
            <h2>Przychodnia Zdrówko (Warszawa, Słoneczna)</h2>
            <p></p>
            <h4>Dostępni specjaliści:</h4>
            <ul>
                <li>Lekarz rodzinny</li>
                <li>Diabetolog</li>
                <li>Endokrynolog</li>
            </ul>
            <h4>Dane kontaktowe:</h4>
            <p><span class="glyphicon glyphicon-globe" aria-hidden="true"></span>   ul. Słoneczna 124, Warszawa</p>
            <p><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>   123456786</p>
        </div>
    </div>
</div>

<footer>
    By Karolina Tomala, 2020
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/main.js"></script>
</body>
</html>


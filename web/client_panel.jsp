<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap-cosmo.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Przychodnia Zdrówko</a>

    <div id="navbarColor02">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.jsp">Strona główna</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="institutions.jsp">Nasze przychodnie</a>
            </li>
            <li class="nav-item active">
                <c:url var="listLink" value="ClientServlet">
                    <c:param name="command" value="LIST"></c:param>
                </c:url>
                <a class="nav-link" href="${listLink}">Panel klienta</a>
            </li>
            <c:url var="logoutLink" value="ClientServlet">
                <c:param name="command" value="LOGOUT"></c:param>
            </c:url>
            <li class="nav-item">
                <a href="${logoutLink}">
                    <button type="button" class="btn btn-danger">Wyloguj</button>
                </a></td>
            </li>
        </ul>
    </div>
</nav>

<div class="jumbotron" id="panel_img">
    <div class="container">
        <h1>Witaj w panelu Klienta!</h1>
    </div>
</div>

<div class="container margin_top_30">
    <h2>Chcesz zarezerwować nową wizytę?</h2>
    <p>Masz możliwość zarezerowania wizyty w każdej z naszych klinik! Pamiętaj jednak, że nie w każdej klinice istnieje możliwość wybrania interesującego Cię specjalisty.</p>
</div>
<div class="container form_card">
    <p>Wypełnij poniższe pola aby zobaczyć dostępne wizyty:</p>
        <form action="VisitReservationServlet" method="post">
            <div class="form-group">
                <label for="institution">Placówka:</label>
                <select name="institution" id="institution">
                    <c:forEach var="tmpInstitution" items="${INSTITUTIONS_LIST}">
                        <option value=${tmpInstitution.id}>${tmpInstitution.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label for="specialization">Specjalizacja:</label>
                <select name="specialization" id="specialization">
                    <c:forEach var="tmpSpecialization" items="${SPECIALIZATIONS_LIST}">
                        <option>${tmpSpecialization}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label for="visitDate">Data:</label>
                <input type="date" class="form-control" name="visitDate" id="visitDate" required>
            </div>
            <button type="submit" class="btn btn-primary btn-lg">Pokaż dostępne wizyty &raquo;</button>
        </form>
</div>

<div class="container margin_top_30 margin_bottom_30">
    <h2>Twoje umówione wizyty:</h2>

    <table class="table">

        <thead>
        <tr>
            <th scope="col">Placówka</th>
            <th scope="col">Data</th>
            <th scope="col">Godzina</th>
            <th scope="col">Specjalizacja</th>
            <th scope="col">Lekarz</th>
            <th scope="col">Telefon kontaktowy</th>
            <th scope="col">Anuluj wizytę</th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="tmpVisits" items="${CLIENT_VISITS_LIST}">

            <c:url var="cancelLink" value="ClientServlet">
                <c:param name="command" value="CANCEL"></c:param>
                <c:param name="visitId" value="${tmpVisits.id}"></c:param>
                <c:param name="userId" value="${tmpVisits.userId}"></c:param>
            </c:url>


            <tr>
                <td>${tmpVisits.institution}</td>
                <td>${tmpVisits.date}</td>
                <td>${tmpVisits.time}</td>
                <td>${tmpVisits.specialization}</td>
                <td>${tmpVisits.doctorName}</td>
                <td>${tmpVisits.doctorPhone}</td>
                <td>
                    <c:if test="${tmpVisits.isAfterNow}">
                        <a href="${cancelLink}"
                           onclick="if(!(confirm('Czy na pewno chcesz anulować tę wizytę?'))) return false">
                            <button type="button" class="btn btn-danger">Anuluj wizytę</button>
                        </a></td>
                    </c:if>
            </tr>

        </c:forEach>
        </tbody>
    </table>

    <p>${message}</p>

</div>

<footer>
    By Karolina Tomala, 2020
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/main.js"></script>
</body>
</html>

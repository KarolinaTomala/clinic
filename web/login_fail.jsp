<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap-cosmo.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Przychodnia Zdrówko</a>

    <div id="navbarColor02">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.jsp">Strona główna</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="institutions.jsp">Nasze przychodnie</a>
            </li>
            <li class="nav-item active">
                <c:url var="listLink" value="ClientServlet">
                    <c:param name="command" value="LIST"></c:param>
                </c:url>
                <a class="nav-link" href="${listLink}">Panel klienta</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container form_card">
    <h2>Niepowodzenie logowania</h2>
    <p>Niestety wprowadzone dane logowania są niepoprawne. Spróbuj ponownie aby w pełni korzystać z możliwości serwisu!</p>

    <a class="btn btn-primary btn-lg btn-block" href="login_form.jsp" role="button">Przejdź ponownie do formularza logowania</a>
</div>

<div class="container form_card">
    <p>Nie jesteś zarejestrowany w naszej przychodni? Załóż konto!</p>
    <a class="btn btn-secondary btn-lg btn-block" href="sign_up_form.jsp" role="button">Zarejestruj się!</a>
</div>

<footer>
    By Karolina Tomala, 2020
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/main.js"></script>
</body>
</html>

